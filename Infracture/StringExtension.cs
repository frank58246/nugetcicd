﻿using System;

namespace Infracture
{
    public static class StringExtension
    {
        public static bool IsNullOrEmpty(this string value)
        { 
            return value == null || value.Length == 0;
        }
    }
}
